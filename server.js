const { exec } = require("child_process");
const { Request } = require("utils-lib-js");
// 替换为你的 Gitee API Token
const access_token = "";
const per_page = 5; // 每页获取的数量，最大为5
const page = 1;
const request = new Request("https://gitee.com/");
const blackList = []; // 黑名单，列表中的不克隆
// 获取所有仓库信息
const getAllRepos = (page) => {
  request
    .GET(`/api/v5/user/repos`, {
      page,
      per_page,
      access_token,
    })
    .then((repos) => {
      if (repos.length > 0) {
        repos.forEach((repo) => {
          const { html_url, name } = repo;
          if (blackList.includes(name)) return console.log(`${name} 已跳过`);
          // 克隆每个仓库到本地
          console.log(`${name} 开始下载`);
          exec(`cd ./repos && git clone ${html_url}`, (cloneErr) => {
            if (cloneErr) return console.error(`克隆出错: ${cloneErr}`);
            console.log(`${name} 克隆完成`);
          });
        });
        // 继续获取下一页的仓库信息
        getAllRepos(page + 1);
      } else {
        console.log("克隆命令执行完毕！");
      }
    })
    .catch(console.log);
};
getAllRepos(page);
