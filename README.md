# git-repos-clone

#### 介绍
基于node实现批量复制自己的代码仓库的脚本


#### 使用说明

1.  pnpm i
2.  获取自己的gitee的token，并输入
3.  pnpm start

#### 参与贡献

1.  Fork 本仓库
2.  Star 仓库
3.  提交评论
4.  新建 Pull Request



